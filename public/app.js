console.log("Loading...");


function color(color) {
  $(".eye-inner").attr("fill", color);
  $(".eye").css("filter", "drop-shadow(0 0 5vh " + color + ")");
}

function move(x, y) {
  $(".eye").css({ transform: "translate(" + x + "vh, " + y + "vh)" });
}

var maskTypeTop = "neutral";
var maskLevelTop = -10;

function angry(level) {
  if (Math.round(maskLevelTop) != -10 && maskTypeTop != "angry") {
    console.warn(
      "It appears that you haven't reset the previous emotion yet. Please consider resetting it before setting a new one."
    );
  }

  var speed = (level - maskLevelTop) / transTime * 10;
  maskTypeTop = "angry";

  console.log("Getting angry at", speed, "px per ms");

  var anim = setInterval(function () {
    if (
      Math.round(level) == Math.round(maskLevelTop) ||
      Math.round(level) + speed == Math.round(maskLevelTop) ||
      Math.round(level) - speed == Math.round(maskLevelTop)
    ) {
      console.log("Angry transition done");
      clearInterval(anim);
    } else {
      maskLevelTop = maskLevelTop + speed;
    }

    $("#eye-mask-top-left").attr(
      "d",
      "M0,0 L0," +
        maskLevelTop +
        " L120," +
        (maskLevelTop + 30) +
        " L120," +
        maskLevelTop +
        " L120,0"
    );
    $("#eye-mask-top-right").attr(
      "d",
      "M0,0 0," +
        (maskLevelTop + 30) +
        " L120," +
        maskLevelTop +
        " L120," +
        maskLevelTop +
        " L120,0"
    );
  }, 10);
}

function sad(level) {
  if (Math.round(maskLevelTop) != -10 && maskTypeTop != "sad") {
    console.warn(
      "It appears that you haven't reset the previous emotion yet. Please consider resetting it before setting a new one."
    );
  }

  var speed = (level - maskLevelTop) / transTime * 10;
  maskTypeTop = "sad";

  console.log("Getting sad at", speed, "px per ms");

  var anim = setInterval(function () {
    if (
      Math.round(level) == Math.round(maskLevelTop) ||
      Math.round(level) + speed == Math.round(maskLevelTop) ||
      Math.round(level) - speed == Math.round(maskLevelTop)
    ) {
      console.log("Sad transition done");
      clearInterval(anim);
    } else {
      maskLevelTop = maskLevelTop + speed;
    }

    $("#eye-mask-top-left").attr(
      "d",
      "M0,0 0," +
        (maskLevelTop + 30) +
        " L120," +
        maskLevelTop +
        " L120," +
        maskLevelTop +
        " L120,0"
    );
    $("#eye-mask-top-right").attr(
      "d",
      "M0,0 L0," +
        maskLevelTop +
        " L120," +
        (maskLevelTop + 30) +
        " L120," +
        maskLevelTop +
        " L120,0"
    );
  }, 10);
}

var maskLevelEmpty = 0;
function emptyEyes(level) {
  var speed = (level - maskLevelEmpty) / transTime * 10;

  var anim = setInterval(function () {
    if (
      Math.round(level) == Math.round(maskLevelEmpty) ||
      Math.round(level) + speed == Math.round(maskLevelEmpty) ||
      Math.round(level) - speed == Math.round(maskLevelEmpty)
    ) {
      $(".empty-eye").attr("r", level);
      console.log("Empty eye transition done");
      clearInterval(anim);
    } else {
      maskLevelEmpty = maskLevelEmpty + speed;
    }

    $(".empty-eye").attr("r", maskLevelEmpty);
  }, 10);
}

var transTime = 100;
function time(ms){
  $("*").css("transition-duration", ms + "ms");
  transTime = ms;
}

function blink(){
  $(".eye").css("height", "0%");
  setTimeout(function(){
    $(".eye").css("height", "100%");
  }, transTime);
}


var animation = null;
function stopAnim(){
  if(animation == null){
    console.error("There's currently not any animation playing");
    return;
  }
  clearInterval(animation);
}

function anim_idle(){
  animation = setInterval(function() {
    move(0,0);
    time(200);
    setTimeout(function(){
      move(15, 0);
    }, 100);
    setTimeout(function(){
      time(400);
      move(-15, 0);
    }, 800);
    setTimeout(function(){
      time(200);
      move(0, 0);
    }, 1700);
    setTimeout(function(){
      time(50);
      blink();
    }, 2000);
    setTimeout(function(){
      blink();
    }, 2300);
  }, 5000);
}


$(window).on("load", function () {
  console.clear();

  console.info(
    "%c -- %cEyes Generator%c --\n    %cby JoLoZs\n\n%c(totally not inspired by Murder drones)",
    "font-size: 2rem",
    "color: #ff00ff; font-size: 2rem",
    "color: inherit; font-size: 2rem",
    "color: cyan; align-text: center",
    "color: gray"
  );
  console.info(
    "%cSpeaking of, you should watch the Pilot at %chttps://www.youtube.com/watch?v=mImFz8mkaHo%c and while you're at it, also watch %chttps://www.youtube.com/watch?v=dQw4w9WgXcQ",
    "color: gray;",
    "color: inherit;",
    "color: gray;",
    "color: inherit;"
  );

  console.info("But let's get started.");

  console.info("\n%cAvailable commands\n", "color: #ff00ff;");

  console.info(
    '%ccolor%c(%c"#xxxxxx"%c); %c- Changes the eye color to the hex code you specify.',
    "color: cyan",
    "color: gray",
    "color: lime",
    "color: gray",
    "color: inherit"
  );

  console.info(
    '%cemptyEyes%c(%c"x"%c); %c- Cuts out the innter part of the eyes, making them look empty. To activate, set %cx%c to 45, to deactivate, set it to 0.',
    "color: cyan",
    "color: gray",
    "color: lime",
    "color: gray",
    "color: inherit",
    "color: lime",
    "color: inherit"
  );

  console.info(
    '%cangry%c(%c"x"%c); %c- Cuts off a part of the eyes at the top to make them look angry. The bigger %cx%c is, the furter down travels the emotion.',
    "color: cyan",
    "color: gray",
    "color: lime",
    "color: gray",
    "color: inherit",
    "color: lime",
    "color: inherit"
  );

  console.info(
    '%csad%c(%c"x"%c); %c- Cuts off a part of the eyes at the top to make them look sad. The bigger %cx%c is, the furter down travels the emotion.',
    "color: cyan",
    "color: gray",
    "color: lime",
    "color: gray",
    "color: inherit",
    "color: lime",
    "color: inherit"
  );

  console.warn(
    "%cEmotions don't mix well.%c To prevent awful looking transitions, disable the latest emotion by running it's command with -10 as %cx%c",
    "font-weight: bold",
    "color: inherit",
    "color: lime",
    "color: inherit"
  );

  console.info(
    '%cmove%c(%cx%c, %cy%c); %c- Moves the eyes to the spot put in',
    "color: cyan",
    "color: gray",
    "color: lime",
    "color: gray",
    "color: lime",
    "color: gray",
    "color: inherit"
  );

  console.info(
    '%ctime%c(%cms%c); %c- Changes how long items take to transition. Default: 100',
    "color: cyan",
    "color: gray",
    "color: lime",
    "color: gray",
    "color: inherit"
  );

  console.info(
    '%cstopAnim%c(%c%c); %c- Stops any built-in animation playing.',
    "color: cyan",
    "color: gray",
    "color: lime",
    "color: gray",
    "color: inherit"
  );

  console.info('As a temporary info, right now the animation `idle` automatically starts after 5 seconds.');
  anim_idle();

  console.info(
    "And that's all, hope you'll have fun with this site, I'll be expanding it for a bit.\n  -JoLoZs"
  );
});
color("#ff00ff");
