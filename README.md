[View live](https://joloz.gitlab.io/eyes/)
# Eyes
A [Murder Drones](https://www.youtube.com/watch?v=mImFz8mkaHo) inspired site displaying two fancy eyes.

Nothing too special, but I felt like sharing it in case someone needs it. Open the JavaScript console for full control over the eyes.
